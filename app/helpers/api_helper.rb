module ApiHelper
  include Spree::Api::ApiHelpers
  @@product_attributes = [
        :id, :name, :description, :price, :display_price, :available_on,
        :slug, :meta_description, :meta_keywords, :shipping_category_id,
        :taxon_ids, :total_on_hand, :attribute_1, :attribute_2
      ]
end
