class AddAttributesToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :attribute_1, :string
    add_column :spree_products, :attribute_2, :string
  end
end
