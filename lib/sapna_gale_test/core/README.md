# sapna_gale_test\_core

Core contains all the models and the majority of the logic for the sapna_gale_test
ecommerce system.


## Testing

Create the test site

    bundle exec rake test_app

Run the tests

    bundle exec rake spec
