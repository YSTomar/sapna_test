describe Spree do
  describe '.sapna_gale_test_version' do
    it "returns a string" do
      expect(Spree.sapna_gale_test_version).to be_a(String)
    end
  end

  describe '.sapna_gale_test_gem_version' do
    it "returns a Gem::Version" do
      expect(Spree.sapna_gale_test_gem_version).to be_a(Gem::Version)
    end

    it "can be compared" do
      expect(Spree.sapna_gale_test_gem_version).to be > Gem::Version.new("1.0")
    end
  end
end
