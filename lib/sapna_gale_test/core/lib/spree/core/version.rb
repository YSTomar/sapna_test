module Spree
  def self.version
    ActiveSupport::Deprecation.warn("Spree.version does not work and will be removed from sapna_gale_test. Use Spree.sapna_gale_test_version instead to determine the sapna_gale_test version")
    "1.0.0"
  end

  def self.sapna_gale_test_version
    "1.0.0"
  end

  def self.sapna_gale_test_gem_version
    Gem::Version.new(sapna_gale_test)
  end
end
