namespace :sapna_gale_test do
  namespace :upgrade do
    desc "Upgrade sapna_gale_test to version 1.0"
    task one_point_three: [
        'sapna_gale_test:migrations:assure_store_on_orders:up',
        'sapna_gale_test:migrations:migrate_shipping_rate_taxes:up'
      ] do
      puts "Your sapna_gale_test install is ready for sapna_gale_test 1.0."
    end
  end
end
