# encoding: UTF-8
require_relative 'core/lib/spree/core/version.rb'

Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'sapna_gale_test'
  s.version     = Spree.sapna_gale_test_version
  s.summary     = 'Full-stack e-commerce framework for Ruby on Rails.'
  s.description = 'sapna gale test is an open source e-commerce framework for Ruby on Rails.'

  s.files        = Dir['README.md', 'lib/**/*']
  s.require_path = 'lib'
  s.requirements << 'none'
  s.required_ruby_version     = '>= 2.1.0'
  s.required_rubygems_version = '>= 1.8.23'

  s.author       = 'Sapna Yajuvendra Tomar'
  s.email        = 'ystomar12488@gmail.com'
  s.license      = 'BSD-3'

  s.add_dependency 'sapna_gale_test_core', s.version
  s.add_dependency 'sapna_gale_test_api', s.version
  s.add_dependency 'sapna_gale_test_backend', s.version
  s.add_dependency 'sapna_gale_test_frontend', s.version
  s.add_dependency 'sapna_gale_test_sample', s.version
end
