# sapna_gale_test\_frontend

Frontend contains controllers and views implementing a storefront and cart for sapna_gale_test.


## Testing

Create the test site

    bundle exec rake test_app

Run the tests

    bundle exec rake spec
