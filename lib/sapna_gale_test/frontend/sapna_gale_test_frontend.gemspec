# encoding: UTF-8
require_relative '../core/lib/spree/core/version.rb'

Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'sapna_gale_test_frontend'
  s.version     = Spree.sapna_gale_test_version
  s.summary     = 'Cart and storefront for the Sapna gale test e-commerce project.'
  s.description = s.summary

  s.required_ruby_version = '>= 2.1.0'
  s.author      = 'Sapna Yajuvendra Tomar'
  s.email       = 'ystomar12488@gmail.com'
  s.rubyforge_project = 'sapna_gale_test_frontend'

  s.files        = `git ls-files`.split("\n")
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'sapna_gale_test_api', s.version
  s.add_dependency 'sapna_gale_test_core', s.version

  s.add_dependency 'canonical-rails', '~> 0.0.4'
  s.add_dependency 'jquery-rails'
  s.add_dependency 'sass-rails'
  s.add_dependency 'coffee-rails'
  s.add_dependency 'font-awesome-rails', '~> 4.0'

  s.add_development_dependency 'capybara-accessible'
end
