# sapna_gale_test\_api

API contains controllers and rabl views implementing the REST API of sapna_gale_test.

## Testing

Create the test site

    bundle exec rake test_app

Run the tests

    bundle exec rake spec
