Rails.application.config.assets.precompile += %w[
  spree/backend/all*
  fontawesome-webfont*
  select2_locale*
  jquery-ui/*.png
  sapna_gale_test-style-guide-logo.png
]
