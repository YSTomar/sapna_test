# encoding: UTF-8
require_relative '../core/lib/spree/core/version.rb'

Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'sapna_gale_test_sample'
  s.version     = Spree.sapna_gale_test_version
  s.summary     = 'Sample data (including images) for use with Sapna gale test.'
  s.description = s.summary

  s.required_ruby_version = '>= 2.1.0'
  s.author      = 'Sapna Yajuvendra Tomar'
  s.email       = 'ystomar12488@gmail.com'
  s.license     = 'BSD-3'

  s.files        = `git ls-files`.split("\n")
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'sapna_gale_test_core', s.version
end
