require 'sapna_gale_test_core'
require 'sapna_gale_test_api'
require 'sapna_gale_test_backend'
require 'sapna_gale_test_frontend'
require 'sapna_gale_test_sample'

begin
  require 'protected_attributes'
  puts "*" * 75
  puts "[FATAL] Spree does not work with the protected_attributes gem installed!"
  puts "You MUST remove this gem from your Gemfile. It is incompatible with Spree."
  puts "*" * 75
  exit
rescue LoadError
end
